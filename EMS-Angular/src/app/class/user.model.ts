
export class User {
  userId: bigint;
  name: string;
  role: string;
  email: string;
  phone: string;
  department: string;
  position: string;
}
