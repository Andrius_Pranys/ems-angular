import { getLocaleDateFormat } from '@angular/common';
import { MAT_DATE_LOCALE } from '@angular/material';

export class Task {
  taskId: bigint;
  userId: bigint;
  description: string;
  status: boolean = true;
  timestamp: string = Date().slice(4, 24);
  deadline: string;

  constructor(
    employeeId: bigint,
    description: string,
    deadline: string
  ) {
    this.userId = employeeId;
    this.description = description;
    this.deadline = deadline;
  }
}
