import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {EmployeeCardComponent} from './components/employee-card/employee-card.component';
import {ListsComponent} from './components/lists/lists.component';
import { DepartmentsComponent } from './components/departments/departments.component';


const routes: Routes = [
  {path: '', component: ListsComponent},
  {path: 'user-tasks/:id', component: EmployeeCardComponent},
  {path: 'department/:departments', component: DepartmentsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
