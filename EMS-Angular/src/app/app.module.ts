import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AllUsersComponent } from './pages/all-users/all-users.component';
import { SingleUserComponent } from './pages/single-user/single-user.component';
import { TaskListComponent } from './components/task-list/task-list.component';
import { ManagerListComponent } from './components/manager-list/manager-list.component';
import { EmployeeListComponent } from './components/employee-list/employee-list.component';
import { EmployeeCardComponent } from './components/employee-card/employee-card.component';
import { HeadComponent } from './common/head/head.component';
import { NavbarComponent } from './common/navbar/navbar.component';
import { FooterComponent } from './common/footer/footer.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatFormFieldModule, MatInputModule,
  MatPaginatorModule, MatIconModule,
  MatDatepickerModule, MatSortModule,
  MatTableModule, MatButtonModule,
  MatCheckboxModule, MatSelectModule,
  MatNativeDateModule, MatTooltipModule
} from '@angular/material';
import { ReactiveFormsModule } from "@angular/forms";
import { TaskService } from './services/task.service';
import { FormsModule } from '@angular/forms';
import { ListsComponent } from './components/lists/lists.component';
import { UserService } from './services/user.service';
import { DepartmentsComponent } from './components/departments/departments.component';
@NgModule({
  declarations: [
    AppComponent,
    AllUsersComponent,
    SingleUserComponent,
    TaskListComponent,
    ManagerListComponent,
    EmployeeListComponent,
    EmployeeCardComponent,
    HeadComponent,
    NavbarComponent,
    FooterComponent,
    ListsComponent,
    DepartmentsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatPaginatorModule,
    MatTableModule,
    MatFormFieldModule,
    MatSortModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSelectModule,
    MatDatepickerModule,
    MatIconModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    FormsModule,
    MatTooltipModule
  ],
  providers: [
    UserService,
    TaskService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
