import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Task} from '../class/task.model';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private baseUrl = 'http://localhost:8080/users/usertasks';
  private baseUrlCrud = 'http://localhost:8080/tasks';


  constructor(private http: HttpClient) { }

  getTasksList(id: bigint): Observable<Task[]> {
    return this.http.get<Task[]>(`${this.baseUrl}/${id}`);
  }

  getTask(id: bigint): Observable<Task> {
    return this.http.get<Task>(`${this.baseUrl}/${id}`);
  }

  createTask(task: Task): Observable<Task> {
    return this.http.post<Task>(`${this.baseUrlCrud}`, task);
  }

  updateTask(id: bigint, value: any): Observable<Task> {
    return this.http.put<Task>(`${this.baseUrlCrud}/${id}`, value);
  }

  deleteTask(id: bigint): Observable<Task> {
    return this.http.delete<Task>(`${this.baseUrlCrud}/${id}`, {responseType: 'json'});
  }
}
