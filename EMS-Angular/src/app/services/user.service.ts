import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../class/user.model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = 'http://localhost:8080/users';
  private baseUrlSingleUser = 'http://localhost:8080/user';

  constructor(private http: HttpClient) {
  }

  getUsersList(role: string): Observable<User[]> {
    return this.http.get<User[]>(`${this.baseUrl}/${role}`);
  }


  getUser(id: bigint): Observable<User> {
    // @ts-ignore
    return this.http.get<User>(`${this.baseUrlSingleUser}/${id}`);
  }

  createUser(task: object): Observable<User> {
    // @ts-ignore
    return this.http.post(`${this.baseUrl}`, task);
  }

  updateUser(id: bigint, value: any): Observable<User> {
    // @ts-ignore
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteUser(id: bigint): Observable<User> {
    return this.http.delete<User>(`${this.baseUrl}/${id}`, {responseType: 'json'});
  }

  getUsersListFromDepartments(department: string): Observable<User[]> {
    return this.http.get<User[]>(`${this.baseUrl}/department/${department}`);
  }
}
