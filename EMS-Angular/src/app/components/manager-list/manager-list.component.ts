import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { User } from 'src/app/class/user.model';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-manager-list',
  templateUrl: './manager-list.component.html',
  styleUrls: ['./manager-list.component.css']
})
export class ManagerListComponent implements OnInit {

  // @ts-ignore
  @ViewChild(MatSort) sort: MatSort;
  // @ts-ignore
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns = ['userId', 'name', 'email', 'phone', 'department', 'position'];
  dataSource;

  constructor(private managerService: UserService,
              private router: Router) {
  }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.managerService.getUsersList("admin").subscribe(result => {
      if (!result) {
        return;
      }
      this.dataSource = new MatTableDataSource(result);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  deleteUser(id: bigint) {
    this.managerService.deleteUser(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  UserDetails(id: bigint) {
    this.router.navigate(['details', id]);
  }

  updateUser(id: bigint) {
    this.router.navigate(['update', id]);
  }

  applyFilter($event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  employeeCardCall(id: bigint) {
    this.router.navigate(['user-tasks', id]);
 }
}
