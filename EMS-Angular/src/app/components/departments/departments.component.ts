import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute } from '@angular/router';
import { MatSort, MatSortable, MatTableDataSource, MatPaginator } from '@angular/material';

@Component({
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.css']
})
export class DepartmentsComponent implements OnInit {

  department: any;
  displayedColumns = ['userId', 'name', 'email', 'phone', 'department', 'position'];
  dataSource;

  constructor(private userService: UserService, private route: ActivatedRoute) { }

  // @ts-ignore
  @ViewChild(MatSort) sort: MatSort;
  // @ts-ignore
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.department = this.route.snapshot.params['departments'];
    this.userService.getUsersListFromDepartments(this.department).subscribe(result => {
      if (!result) {
        return;
      }
      this.dataSource = new MatTableDataSource(result);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }
}
