import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { EmployeeListComponent } from '../employee-list/employee-list.component';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { TaskService } from 'src/app/services/task.service';
import { MatSort, MatSortable, MatTableDataSource, MatPaginator, DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatDatepicker } from '@angular/material';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Task } from 'src/app/class/task.model';
import { User } from 'src/app/class/user.model';
import { FormGroup, FormControl } from '@angular/forms';
import { Z_UNKNOWN } from 'zlib';


@Component({
  selector: 'app-employee-card',
  templateUrl: './employee-card.component.html',
  styleUrls: ['./employee-card.component.css'],
  providers: [{ provide: MAT_DATE_LOCALE, useValue: 'lt-LT' }],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class EmployeeCardComponent implements OnInit {

  taskDescription: string;
  taskDeadline: string;
  employeeId: bigint = this.route.snapshot.params['id'];
  task: Task;
  taskId: bigint;
  user: User;
  expandedElement: Task | null;
  taskSource;
  submitted: boolean = false;
  newTask: Task;
  employeesSource;
  cssId: string;
  displayedColumns = ['taskId', 'description', 'timestamp', 'deadline', 'status'];

  // @ts-ignore
  @ViewChild(MatSort) sort: MatSort;
  // @ts-ignore
  @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ts-ignore
  @ViewChild(MatDatepicker) datepicker: MatDatepicker<Date>;

  constructor(
    private userService: UserService,
    private taskService: TaskService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.reloadData();
  }


  reloadData() {
    this.employeeId = this.route.snapshot.params['id'];
    this.taskService.getTasksList(this.employeeId).subscribe(result => {
      if (!result) {
        return;
      }
      this.taskSource = new MatTableDataSource(result);
      this.taskSource.sort = this.sort;
      this.taskSource.paginator = this.paginator;
      this.taskSource.datepicker = this.datepicker;
    });

    this.user = new User();

    this.userService.getUser(this.employeeId)
      .subscribe(data => {
        console.log(data)
        this.user = data;
      }, error => console.log(error));
  }


  applyFilter($event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.taskSource.filter = filterValue.trim().toLowerCase();
  }


  // Default value for task if no new value is entered.
  saveDataVar(defaultDescription: string, defaultDeadline: string) {
    this.taskUpdateForm.patchValue({
      description: defaultDescription,
      deadline: defaultDeadline
    })
  }

  taskUpdateForm = new FormGroup({
    description: new FormControl(),
    deadline: new FormControl()
  });

  updateTask(id: bigint) {
    this.task = this.taskUpdateForm.value;
    this.taskService.updateTask(id, this.task)
      .subscribe(data => {
        console.log(data),
          error => console.log(error);
        this.reloadData()
      });
    console.log(id);
  }

  deleteTask(id: bigint) {
    this.taskService.deleteTask(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  taskCreateForm = new FormGroup({
    description: new FormControl(),
    deadline: new FormControl()
  })


  createTask() {

    if (true) {
      this.submitted = true;
      window.setTimeout(() => this.submitted = false, 2000) // the time you want 
    }
    this.employeeId = this.route.snapshot.params['id'];
    this.newTask = new Task(
      this.employeeId,
      this.taskCreateForm.controls['description'].value,
      this.taskCreateForm.controls['deadline'].value
    );
    this.taskService.createTask(this.newTask).subscribe(data => {
      console.log(data), (error: any) => console.log(error);
      this.reloadData();
    });
    this.taskCreateForm.reset();
  }
}
