import {Component, OnInit, ViewChild, Output} from '@angular/core';
import {Router} from '@angular/router';
import {MatSort, MatSortable, MatTableDataSource, MatPaginator} from '@angular/material';
import {UserService} from '../../services/user.service';
import { User } from 'src/app/class/user.model';
import { EventEmitter } from 'protractor';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {


  constructor(private employeeService: UserService,
              private router: Router) {
  }

  // @ts-ignore
  @ViewChild(MatSort) sort: MatSort;
  // @ts-ignore
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns = ['userId', 'name', 'email', 'phone', 'department', 'position'];
  dataSource;


  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.employeeService.getUsersList("user").subscribe(result => {
      if (!result) {
        return;
      }
      this.dataSource = new MatTableDataSource(result);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  deleteEmployee(id: bigint) {
    this.employeeService.deleteUser(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  EmployeeDetails(id: bigint) {
    this.router.navigate(['details', id]);
  }

  updateEmployee(id: bigint) {
    this.router.navigate(['update', id]);
  }

  applyFilter($event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

   employeeCardCall(id: bigint) {
     this.router.navigate(['user-tasks', id]);
  }
 
}
